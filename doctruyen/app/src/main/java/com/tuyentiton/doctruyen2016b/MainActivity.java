package com.tuyentiton.doctruyen2016b;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.tuyentiton.doctruyen2016b.ui.CustomActionBar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private CustomActionBar actionBar;
    private DrawerLayout drawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        actionBar = (CustomActionBar)findViewById(R.id.customActionBar);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
        initView();
    }

    private void initView(){
        actionBar.setShowType(CustomActionBar.SHOW_TITLE);
        actionBar.setTvTitle(getString(R.string.app_name));
        actionBar.setNaviListener(new CustomActionBar.NaviListener() {
            @Override
            public void onRightClick() {
                if(actionBar.isSearch()){
                    actionBar.setShowType(CustomActionBar.SHOW_TITLE);
                    actionBar.setImgRight(R.drawable.ic_search);
                }else {
                    actionBar.setShowType(CustomActionBar.SHOW_SEARCH);
                    actionBar.setImgRight(R.drawable.cancel);
                }
            }

            @Override
            public void onLeftClick() {
                openDrawer();
            }
        });
    }
    private void closeDrawer(){
        if(drawerLayout.isDrawerVisible(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void openDrawer(){
        if(!drawerLayout.isDrawerVisible(GravityCompat.START)){
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onClick(View v) {

    }
}
